
//var  app = angular.module("appProveedores",[])

var  app = angular.module('appProveedores', ["ui.bootstrap.modal"])
	.config([
			function($interpolateProvider){
		    	$interpolateProvider.startSymbol('[[').endSymbol(']]');
			}
		]);

app.controller("tablas", function( $scope, $http, $rootScope ){
	$scope.datos = [];
	$scope.detalles;

	$scope.loading = false;

	$scope.filtro = function(datos)
	{
		valido = false;

		valido |= datos.Proveedor.match(/^Ma/);
		valido |= datos.Clave.match(/^Ma/);
		valido |= datos.RFC.match(/^Ma/);

		return valido;
	};

	// Llamadas iniciales
	$scope.init = function(){
		//$scope.consultaProveedores();
	}


	/*
	$scope.consultaProveedores = function(){
		$scope.loading = true;

		$http.get("/api/adquisiciones/proveedor/")
		.success(function(data){
			$scope.loading = false;
			// Validar respuesta JSON
			$scope.datos = data.respuesta.Proveedores;

		})
		.error(function(err){
			$scope.loading = false;
		});

		$scope.verListaProveedores = true;
	}

	$scope.editar = function(event) {
		$scope.loading = true;
		$http.get("/api/adquisiciones/detalles/proveedor/?busqueda=" + event)
			.success(function(data){
			// Validar respuesta JSON
			$scope.detalles = data.respuesta.Proveedores[0];
			$scope.loading = false;
			$scope.showModal = true;
		})
		.error(function(err){
			alert('error');
			$scope.loading = false;
		});
	};
	*/

	$scope.modificar = function() {
		$scope.datos = [];
		$scope.showModal = false;
		$scope.verListaProveedores = false;
		$rootScope.$emit("formularioNuevo", { mostrar : true, idProveedor : 10 });
	};

	$scope.eliminar = function() {
		alert("Eliminar proveedor");
		$scope.showModal = false;
	};

	$scope.cerrar = function() {
		$scope.showModal = false;
	};

    $rootScope.$on("generarLista", function(event, args)
    {
		$scope.verListaProveedores = args.mostrar;
    	if (args.mostrar) $scope.consultaProveedores();
    	else $scope.datos = [];    	
    });

});

app.controller("formularioProveedor", function( $scope, $rootScope){
	$scope.verFormularioProveedores = false;
	$scope.idProveedor = 0;

    $rootScope.$on("formularioNuevo", function(event, args)
    {
    	$scope.verFormularioProveedores = args.mostrar;
    	$scope.idProveedor = args.idProveedor;
    });

});

app.controller("accionesAplicacion", function( $scope, $rootScope){
	$scope.listarProveedores = function(){
	   $rootScope.$emit("generarLista", { mostrar : true });
	   $rootScope.$emit("formularioNuevo", { mostrar : false, idProveedor : 0 });
	};

	$scope.agregarProveedor = function(){
	   $rootScope.$emit("generarLista", { mostrar : false });
	   $rootScope.$emit("formularioNuevo", { mostrar : true, idProveedor : 0 });
	};

});




function FormHelper()
{
	this.data = ""; 
	this.append = function( name, val )
	{
		if (this.data.length > 0) { this.data += "&"; }
		this.data += encodeURIComponent(name);
		this.data += "=";
		this.data += encodeURIComponent(val);
	}
}