<?php
namespace Controlador\Respuestas;
class Respuesta
{
    public $tipo;
    public $datos = array();
    public $seccion;
    public $plantilla;

    function __construct($plantilla = null, $datos = array(), $seccion = null, $tipo = "html")
    {
        $this->tipo = $tipo;
        $this->datos = $datos;
        $this->seccion = $seccion;
        $this->plantilla = $plantilla;
    }

}
?>
