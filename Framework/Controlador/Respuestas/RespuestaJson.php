<?php
namespace Controlador\Respuestas;
class RespuestaJson
{
    public $datos;
    public $seccion;

    function __construct($datos = null, $seccion = null)
    {
    	header('Content-type:application/json;charset=utf-8');

    	#Generar IdToken por cada una de las llamadas
    	# Guardar en sesion para comprobar si es la misma sesion
    	$datos['idToken'] = $_SESSION['idToken'] = \Herramientas\Herramientas::randomString(32);


        $this->datos = json_encode( $datos );
        $this->seccion = $seccion;
    }

}
?>
