<?php
namespace Controlador\Respuestas;
class RespuestaPdf
{
    public $datos;

    function __construct( $url, $datos = null )
    {

        ob_start();

        echo '
        <style>
        .watermark1 {
              position: fixed;
              top: 35%;
              left: 105px;
              transform: rotate(-45deg);
              transform-origin: 50% 50%;
              opacity: .2;
              font-size: 120px;
              color: black;
              width: 480px;
              text-align: center;
              font-family:"Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", "DejaVu Sans", Verdana, sans-serif;
            }
            </style>
            ';

        echo '<div class="watermark1">DEMO</div>';

        require( $url );
        $html = ob_get_contents();
        ob_end_clean();

        $dompdf = new DOMPDF();
        $dompdf->set_paper("A4", "portrait");      
        
        

        # Cargamos el contenido HTML.
        $dompdf->load_html( mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8') );         
        
        # Renderizamos el documento PDF.
        $dompdf->render();

        $canvas = $dompdf->get_canvas();
        
        # Se agrega numero de pagina
        $font = Font_Metrics::get_font("helvetica", "bold"); 
        $canvas->page_text(270, 800, "{PAGE_NUM} de {PAGE_COUNT}",$font, 8, array(0,0,0)); 

        # ________________________________________________________________
        # MENSAJE DEMO
        /*
        $w = $canvas->get_width();
        $h = $canvas->get_height();
        $canvas->page_text( $w / 2 -100, $h/2 + 200, "DEMO", $font,110, array(0.85, 0.85, 0.85), 0, 0, -45);
        */
        #-----------------------------------------------------------------

        $dompdf->stream("voucher.pdf", array("Attachment" => 0));

    }

}
?>
