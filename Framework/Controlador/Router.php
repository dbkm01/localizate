<?php 
/**
	SAM - Router V.3
*/
class Router
{
	private $_rutas = array(
			"GET" => array(),
			"POST" => array(),
			"PUT" => array(),
			"DELETE" => array()
		);

	public $valor = "";

	function __construct() { }

	#Agrega uris a el router
	public function add($uri, $clase, $funcion, $tipoRequest)
	{
		$datos = new stdClass();

		$datos->uri = trim($uri, '/');
		$datos->clase = $clase;
		$datos->funcion = $funcion;
		$datos->tipoRequest = $tipoRequest;

		array_push( $this->_rutas[$datos->tipoRequest], $datos );
	}

	# Agregar metodos POST al router.
	public function POST($uri, $clase, $funcion)
	{
		$this->add($uri, $clase, $funcion, "POST");
	}

	# Agregar metodos GET al router.
	public function GET($uri, $clase, $funcion)
	{
		$this->add($uri, $clase, $funcion, "GET");
	}

	# Agregar metodos PUT al router.
	public function PUT($uri, $clase, $funcion)
	{
		$this->add($uri, $clase, $funcion, "PUT");
	}

	# Agregar metodos DELETE al router.
	public function DELETE($uri, $clase, $funcion)
	{
		$this->add($uri, $clase, $funcion, "DELETE");
	}


	# Analiza todas las rutas registradas en el sitio para generar
	# la respuesta.
	public function proceso()
	{
		if ($_SERVER["REQUEST_METHOD"] == "POST") 
			$metodo = isset( $_POST["__metodo"] ) ? $_POST["__metodo"] : "POST";
		else
			$metodo = $_SERVER["REQUEST_METHOD"];

		$uri = isset($_GET['uri']) ? $_GET['uri'] : '/';

		$respuesta = new stdClass();

		foreach ($this->_rutas[$metodo] as $ruta)
		{
			if(preg_match("#^$ruta->uri/?$#", $uri))
			{
				$respuesta = $ruta;
				break;
			}
		}

		return $respuesta;
	}


}


 ?>