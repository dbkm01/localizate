<?php 
	# CONTIENE TODAS LAS LIBRERIAS, CONFIGURACIONES, PROCESOS E INFORMACION
	# DE INICIO.
	# ULTIMA ACTUALIZACION: 06 Marzo 2016
	
	#CONTROL DE SESION DEL USUARIO
	#session_cache_limiter("private");
	header("Expires: Sat, 01 Jan 2000 00:00:00 GMT"); 
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT"); 
	header("Cache-Control: post-check=0, pre-check=0",false); 
	session_cache_limiter("must-revalidate");

	session_start();

	# Datos e informacion importante (constantes,accesos,etc...)
	require("Framework/Configuracion.php");


	# Librerias Generales
	require("Framework/Herramientas/Db/Db.php");
	# require("Framework/Herramientas/DD/Dbclass.php");

	require_once("Framework/Herramientas/Twig/lib/Twig/Autoloader.php");
	Twig_Autoloader::register(true);

	# ROUTER
	require("Framework/Rutas.php");

/*
	if ( $_SESSION['logueado'] )
	{
*/
		$ejecutar = $router->proceso();
		include("Controlador/autoloader.php");

		if ( isset($ejecutar->clase) )
		{
			$clase = new $ejecutar->clase;
			
			if( DEBUG ) $respuesta = call_user_func(array(&$clase, $ejecutar->funcion));
			else
			{
				try
				{
					$respuesta = call_user_func(array(&$clase, $ejecutar->funcion));	
				}
				catch (Exception $e)
				{
					\Herramientas\Log::guardarReporte('errores',"Error interno: \n".$e->getMessage(),'Excepciones');
					die("Error interno");
				}

			}
			
		}
		else
		{
			\Herramientas\Log::guardarReporte('errores',"metodo y clase no existen",'Excepciones');
			die("Error en la llamada"); # Mandar a llamar una vista de error
		}
/*
	}
	else
	{
		$respuesta = Acceso::acceso();
	}
*/

	#RESPUESTA - Reflector para obtener el nombre de la clase sin namespace
	switch ( get_class($respuesta) )
	{
		case 'Controlador\Respuestas\Respuesta':

			$loader = new Twig_Loader_Filesystem('Framework/Vistas/');
			$twig = new Twig_Environment($loader);

			# Ver a donde puedo mover esto.
			# Funciones Twig
			$function = new Twig_SimpleFunction('baseMedia', function ()
			{
			    echo "/public";
			});
			$twig->addFunction($function);

			$function = new Twig_SimpleFunction('uriActivo', function($nivel, $palabra)
			{
				$uri = explode('/', isset($_GET['uri']) ? $_GET['uri'] : '/');
				return preg_match("#^$palabra$#i", $uri[$nivel]) ? "active" : "";
			});
			$twig->addFunction($function);

			echo $twig->render($respuesta->plantilla, $respuesta->datos);

			break;
		case 'Controlador\Respuestas\RespuestaJson':
			# Agregar validacion a respuestas Json
			echo $respuesta->datos;
			break;
		case 'Controlador\Respuestas\RespuestaPdf':
			#echo $respuesta->datos;
			break;
		
		default: /* ERROR */ break;
	}

 ?>
