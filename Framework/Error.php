<?php 
# JALAPEÑO TOURS
# Objeto errores del sistema.
# Ultima Actualización: 23 Junio 2015
Class Error
{
	private $id;
	private $mensaje;
	private $mostrar;
	//private $temp;

	function Error($id = 0, $mensaje = "Error no detectado", $mostrar = false)
	{
		$this->id = $id;
		$this->mensaje = $mensaje;
		$this->mostrar = $mostrar;
	}

	function obtenerID()
	{
		return $this->id;
	}

	function obtenerMensaje()
	{
		return $this->mensaje;
	}

	# Error visible para el usuario? return (bool)
	function mostrar()
	{
		return $this->mostrar;
	}
	
}
	
?>