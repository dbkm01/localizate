<?php
	
	# Necesario para ejecutar el sistema
	require_once('Controlador/Router.php');
	$router = new Router();

	# ----------------------------------------------------------------------------------
	# URI's disponibles
	include("Modulos/Ubicacion/Rutas.php");
	include("Modulos/Web/Rutas.php");

	# ----------------------------------------------------------------------------------

	/*
	
		Ejemplo

		$modulo = '\Adquisiciones\Proveedores';	
		$router->add('proveedores/',	$modulo.'\Proveedores',	'inicio',	'GET');

		$router->add('contactos/',	$modulo.'\Contacto',	'inicio',	'GET');
		$router->add('contactos/',	$modulo.'\Contacto',	'adios',	'POST');
		$router->add('contactos/',	$modulo.'\Contacto',	'adios',	'PUT');
		$router->add('contactos/',	$modulo.'\Contacto',	'comida',	'DELETE');
	
	*/

	?>