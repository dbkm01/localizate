<?php 

	/**
	* Proveedores.
	* 
	*/
	namespace Modulos\Web\Ubicacion;

	# Herramientas
	use \Herramientas\Log as Log;
	use \Herramientas\BaseDeDatos as BaseDeDatos;
	# Respuestas disponibles
	use \Controlador\Respuestas\Respuesta as Respuesta;

	class Ubicacion
	{	
		private $seccion = 'Ubicacion';
		function __construct() { }

		public function historial() 
		{
			$plantilla = "Ubicacion/ubicacion.html";
			$datos = array( );

			return new Respuesta($plantilla, $datos, $this->seccion);
		}


		public function visor() 
		{
			$plantilla = "Ubicacion/mapa.html";
			$datos = array( );

			return new Respuesta($plantilla, $datos, $this->seccion);
		}

	}



 ?>