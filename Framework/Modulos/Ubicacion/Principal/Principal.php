<?php
	/**
	* Metodos relacionados con la vista inicial del sitio.
	* 
	*/
	namespace Modulos\Ubicacion\Principal;
	

	# Herramientas
	use \Herramientas\Log as Log;
	use \Herramientas\Db\BaseDeDatos as db;

	# Respuestas disponibles
	use \Controlador\Respuestas\RespuestaJson 	as RespuestaJson;
	use \Controlador\Respuestas\Respuesta 		as Respuesta;
	
	class Principal
	{
		private $seccion = 'Ubicacion';
		
		function __construct() { }

		public function obtener()
		{
			$db = new db();
			$errores = array();

			$parametros = (object)$_GET;

			$parametros->fechaInit = \Herramientas\Herramientas::formatoFechaBd($parametros->fechaInit);
			$parametros->fechaEnd = \Herramientas\Herramientas::formatoFechaBd($parametros->fechaEnd);

			$bind = array(
					":idUnidad" => intval($parametros->idUnidad),
					":fechaInit" => "$parametros->fechaInit",
					":fechaEnd" => "$parametros->fechaEnd",
					":horaInit" => "$parametros->horaInit",
					":horaEnd" => "$parametros->horaEnd"
				);
			$where = "idUnidad = :idUnidad AND (hora BETWEEN :horaInit AND :horaEnd) AND (fecha BETWEEN :fechaInit AND :fechaEnd)";

			$results = $db->select("historial_ubicacion", $where, $bind);

			$datos = array(
					"Ubicacion" => $results,
					'Fecha' => date('l jS \of F Y h:i:s A')
				);

			return new RespuestaJson(
					array( "respuesta" => $datos, "errores" => $errores ), $this->seccion
				);

		}

		public function agregar()
		{
			$errores = array();
			$post = file_get_contents('php://input');
			$ubicacion = json_decode($post);

			$db = new db();
			$insert = array(
				"longitud" => $ubicacion->longitud,
				"latitud" => $ubicacion->latitud,
				"altitud" => $ubicacion->altitud,
				"velocidad" => $ubicacion->velocidad,
				"idUnidad" => $ubicacion->idUnidad,
				"fecha" => "$ubicacion->fecha",
				"hora" => "$ubicacion->hora",
				);

			$db->insert("historial_ubicacion", $insert);

			$datos = array( "Ubicacion" => 'Ok' );

			return new RespuestaJson(
					array( "respuesta" => $datos, "errores" => array() ),
					$this->seccion
				);

		}


	}
 ?>