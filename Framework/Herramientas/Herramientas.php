<?php
# CONTIENE TODAS LAS FUNCIONES GENERALES 
# QUE PUEDEN SER UTILIZADAS EN CUALQUIER PARTE DEL SISTEMA

# ULTIMA ACTUALIZACION: 24 Junio 2015    
namespace Herramientas;
class Herramientas
{
    # Constructor
	public function Herramientas()
    {
    }
    	
	public static function objetoParseArray($Objeto)
	{
		$respuesta = new stdclass();
		$respuesta->tipo=0;
		$respuesta->resultado=0;

		if(is_object($Objeto))
		{
			if(!is_array($Objeto))
			{
				$arreglo=array();
				array_push($arreglo,$Objeto);
				return $arreglo;
			}
		}
		return $Objeto;
	}

    # El primer parametro, es una fecha en formato date(d-m-y).
    # El segundo parametro es para sumar o quitar dias a la fecha.
    public static function obtenerFecha($fecha,$numeroDeDias)
    {
        list($dia,$mes,$ano) = explode('/',$fecha);

        return date('d/m/Y',mktime(0,0,0,$mes,$dia+$numeroDeDias,$ano));        
    }

    #Convierte a formato para la BD
    public static function formatoFechaBd($fecha)
    {
        list($ano,$mes,$dia) = explode('/',$fecha);

        return date( 'Y/m/d', mktime(0,0,0,$mes,$dia,$ano) );        
    }

    # CONVIERTE UNA CANTIDAD EN FORMATO DE CENTAVOS A PESOS 
    # Ejemplo: 34800 centavos serán 348 pesos
    public static function centavosAPesos($PrecioEnCentavos)
    {
        if($PrecioEnCentavos>0)
        {
            $resultadoXML=$PrecioEnCentavos/100;
        }
        
        return $resultadoXML;
    }


    #Genera una cadena aleatoria.
    public static function randomString( $chars = 8 )
    {
        $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        return substr(str_shuffle($letters), 0, $chars);
    }
	
	# Formato de moneda para paypal.
	public static function formatoMonedaPaypal($monto)
	{		
		$montoFormateado=number_format($monto,2,'.','');
		
		return $montoFormateado;
	}
	
	# Calcular montos con Impuestos
	public static function montoConImpuestos($monto)
	{
		$respuesta=new stdclass();
		$respuesta->porcentaje=0;
		$respuesta->impuestos=0;
		$respuesta->montoConImpuestos=$monto;
		
		$precioConIVA=$monto;
		
		if(isset($_SESSION['IVA']))
		{
			# Calcular IVA
			if($monto>0)
			{
				$iva=$_SESSION['IVA'];
				$totalIVA=($iva*$monto)/100;
				$precioConIVA=$monto+$totalIVA;
				
				$respuesta->porcentaje=$iva;
				$respuesta->impuestos=$totalIVA;
				$respuesta->montoConImpuestos=$precioConIVA;
			}
		}
		
		return $respuesta;
	}

}
?>
