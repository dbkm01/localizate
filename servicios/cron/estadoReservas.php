<?php 
	
    #CONTROL DE SESION DEL USUARIO
    session_start();

    $RUTA = $_SERVER["DOCUMENT_ROOT"]."/reservas/";

    # Incluyendo Datos e informacion importante (constantes,accesos,etc...)
	include($RUTA."framework/Configuracion.php");
	# Incluyendo librerias Generales
	include($RUTA."servicios/ServicioOtisa.php");
    include($RUTA."framework/Log.php");
	include($RUTA."framework/VariablesGlobales.php");
	include($RUTA."framework/Herramientas.php");
	include($RUTA."framework/Db.php");

	# INSTANCIA DE OBJETOS
	$variablesGlobales = new VariablesGlobales();
	
    # Inicio sesion - WEBSERVICE
    include($_SERVER["DOCUMENT_ROOT"]."/reservas/"."framework/inicioSesion.php");

	$webServices = new ServicioOtisa();
	
	$db = new BaseDeDatos(); 
	$reservas = $db->getArray("SELECT idReserva_jalapeno, idReserva_b2b FROM confirmacion_reservas WHERE estado_reserva NOT IN (1, 2)");
	foreach ($reservas as $reserva)
	{
		$respuesta = new stdclass();
		$comprobacion = $webServices->comprobarError();

		# Comprobar Existencia de Error.
		if($comprobacion->resultado)
		{
			$respuesta->datos = array();
			$respuesta->error = TRUE;
			$respuesta->mensajeDeError = NOTIFICACION_ERROR_SISTEMA; 
			Notificaciones::agregarNotificacion('alert-danger',NOTIFICACION_ERROR_SISTEMA);
			Log::guardarReporte('errores',$comprobacion->error,'Excepciones');          
		}
		else
		{
			$resultado=Herramientas::validarRespuestaXML('SECCION: CRON',$webServices->estatusReserva( $reserva['idReserva_b2b'], $_SESSION['idSession'] ));
			$respuesta->datos = $resultado;
			$respuesta->error = FALSE;
			$respuesta->mensajeDeError = "";
		}

		$estado = 0;
		switch ($resultado->EstadoReservaRespuesta->Estado)
		{
			case 'PreReserva': $estado = 0; break;
			case 'Comprada': $estado = 1; break;
			case 'Cancelada': $estado = 2; break;
			
			default: $estado = 0; break;
		}
		
		$db->qqUpdate("confirmacion_reservas", array(
				'idReserva_jalapeno' => $reserva['idReserva_jalapeno'],
				'idReserva_b2b' => $reserva['idReserva_b2b'],
				'estado_reserva' => $estado
				)
			);

	}

	unset($respuesta);

?>